import Home from "./Home";
import Splash from "./Splash";
import MovieDetail from "./MovieDetail";

export {Splash, Home,MovieDetail};