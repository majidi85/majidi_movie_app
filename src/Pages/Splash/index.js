import React, {useEffect} from 'react';
import {StyleSheet, Text, View, ImageBackground, Image} from 'react-native';
import {
  Logo,
  Majidi,
  NetroCinema,
  NetroCinema2,
  Shadow,
  SplashBackground,
} from '../../Assets';
import {
  WARNA_1,
  WARNA_2,
  WARNA_3,
  WARNA_4,
  WARNA_5,
  WARNA_6,
  WARNA_7,
} from '../../Utils/constant';
import LinearGradient from 'react-native-linear-gradient';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout( () => {
        navigation.replace('Home');
    }, 3000)
  }, [navigation]);

  return (
    <ImageBackground source={SplashBackground} style={styles.ImageBackground}>
      <View style={styles.nameAPP}>
        <Image source={Logo} style={styles.logo} />
        <Image source={Shadow} style={styles.shadow} />
        <Image source={NetroCinema}/>
      </View>
      <View style={styles.myname}>
        <Image source={Majidi}/>
      </View>
    </ImageBackground>
  );
};

export default Splash;

const styles = StyleSheet.create({
  ImageBackground: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  logo: {
    justifyContent: 'center',
    width: 219.31,
    height: 176.65,
  },
  shadow: {
    justifyContent: 'center',
    width: 253.27,
    height: 34.4,
  },
  nameAPP: {
    justifyContent: 'center',
    flex: 10,
  },
  myname: {
    marginBottom: 20,
    flex: 1,
    justifyContent: 'center',
  },
});
