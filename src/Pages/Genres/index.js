import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import axios from 'axios';

class Genres extends React.Component {
  constructor() {
    super();
    this.state = {
      dataGenres: [],
      isLoading: false,
    };
  }

  async componentDidMount() {
    this.setState({isLoading: true});
    const serverGenres = await axios.get(
      `http://code.aldipee.com/api/v1/movies/`
    );
   
    this.setState({
      dataGenres: serverGenres.data.genres
    });
    this.setState({isLoading: false});
  }

  render(){
    console.log('dataGenres : ',this.state.dataGenres)
    return (
      <View style={styles.container}>
        {/* {this.state.dataGenres.map(item => (
          <View key={item.id} style={styles.genres}>
            <Text style={styles.textGenre}>{item.name}</Text> */}
          </View>
        // ))}

    )
    
  }
}
  

export default Genres;

const styles = StyleSheet.create({
  container:{
    flex:1,
    flexDirection:'row',
  },
  genres:{
    flex:1,
    marginLeft:15
  },
  textGenre:{
    fontSize:12
  }
})
