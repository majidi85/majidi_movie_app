import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  ActivityIndicator,
  TouchableOpacity,
  Button,
  Dimensions,
} from 'react-native';
import postman from 'axios';
import moment from 'moment';
import Icon from 'react-native-vector-icons/Ionicons';
import {Logo, NetroCinema2} from '../../Assets';
import { PickerItem } from 'react-native/Libraries/Components/Picker/Picker';
import Genres from '../Genres';

class Home extends React.Component {
  constructor() {
    super();
    this.state = {
      dataMovie: [],
      isLoading: false,
    };
  }

  async componentDidMount() {
    this.setState({isLoading: true});
    const dataServer = await postman.get(
      'http://code.aldipee.com/api/v1/movies',
    );
    this.setState({isLoading: false});
    this.setState({dataMovie: dataServer.data.results});
  }

  render() {
    const {dataMovie, isLoading} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <View style={styles.headerLeft}>
            <Image source={Logo} style={styles.logo} />
          </View>
          <View style={styles.headerRight}>
            <Image source={NetroCinema2} />
          </View>
        </View>
        <View style={styles.main}>
          <Text style={styles.textRecommended}>Recommended</Text>
          <ScrollView
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={styles.ScrollRecommended}>
            {this.state.dataMovie.sort((a,b) => a.vote_average < b.vote_average ? 1 : -1).map(item => (
              <TouchableOpacity
                key={item.id}
                onPress={() => {
                  this.props.navigation.navigate('MovieDetail', {
                    idFilm: item.id,
                  });
                }}>
                <View style={styles.listData}>
                
                  <Image
                    style={styles.PosterMovie}
                    source={{uri: `${item.poster_path}`}}
                  />
                
                </View>
              </TouchableOpacity>
              
            ))}
          </ScrollView>
        </View>
        <View style={styles.footer}>
          <Text style={styles.textLatest}>Latest Upload</Text>
          <ScrollView showsVerticalScrollIndicator={false}>
            {this.state.dataMovie.sort((a,b)=>a.release_date < b.release_date ? 1 : -1).map(item => (
              <View key={item.id} style={styles.listMovie}>
                <View style={styles.viewBackdrop}>
                  <Image
                    style={styles.BackdropMovie}
                    source={{uri: `${item.backdrop_path}`}}
                  />
                </View>
                <View style={styles.descript}>
                  <Text style={styles.JudulMovie}>{item.original_title}</Text>
                  <Text style={styles.ReleaseDate}>
                    Release Date :{' '}
                    {moment(item.release_date).format('DD MMM YYYY')}
                  </Text>
                  <View style={{flexDirection: 'row'}}>
                    <Icon name="star" style={styles.star} />
                    <Text style={styles.rating}>{item.vote_average}</Text>
                  </View>
                  <Text style={styles.Genre}>Genre : {item.genre_ids}</Text>
                  {/* <Genres /> */}
                </View>
                <View style={styles.iconbar}>
                  <TouchableOpacity
                    onPress={() => {
                      this.props.navigation.navigate('MovieDetail', {
                        idFilm: item.id,
                      });
                    }}>
                    <Icon
                      name="arrow-forward-circle"
                      style={styles.iconDetail}
                    />
                  </TouchableOpacity>
                </View>
              </View>
            ))}
          </ScrollView>
        </View>
      </View>
    );
  }
}

export default Home;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#749498',
  },
  headerLeft: {
    flex: 1,
  },
  headerRight: {
    flex: 3,
    justifyContent: 'center',
  },
  logo: {
    width: 73.62,
    height: 55,
    margin: 10,
  },
  main: {
    flex: 3,
    padding: 5,
    backgroundColor: '#749498',
  },
  textRecommended: {
    fontFamily: 'Open Sans',
    fontWeight: '900',
    fontSize: 21,
    marginLeft: 15,
  },
  footer: {
    flex: 4,
    flexDirection: 'column',
    backgroundColor: '#749498',
  },
  PosterMovie: {
    height: 169.5,
    width: 112.5,
    borderRadius: 15,
    borderColor: 'grey',
    overflow: 'hidden',
    margin: 10,
    resizeMode: 'contain',
  },
  textLatest: {
    fontFamily: 'Open Sans',
    fontWeight: '900',
    fontSize: 21,
    marginLeft: 15,
  },
  listMovie: {
    margin: 10,
    flex: 1,
    flexDirection: 'row',
    borderRadius: 15,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.19,
    shadowRadius: 4.65,
    paddingLeft: 15,
    backgroundColor: 'rgba(21,21,21,0.5)',
  },
  viewBackdrop: {
    flex: 2,
  },
  BackdropMovie: {
    height: 90,
    width: 160,
    borderRadius: 15,
    borderWidth: 0.2,
    borderColor: 'grey',
    overflow: 'hidden',
    marginLeft: -15,
  },
  descript: {
    flex: 2,
    marginLeft: 5,
    justifyContent: 'center',
  },
  JudulMovie: {
    fontSize: 16,
    fontWeight: 'bold',
    color:'#DEDDDF',
  },
  ReleaseDate: {
    color:'#DEDDDF',
  },
  Genre: {
    fontSize: 14,
  },
  star: {
    color: '#F99601',
    justifyContent: 'space-evenly',
    marginRight: 2,
  },
  rating: {
    marginLeft: 5,
    color:'#DEDDDF',
  },
  iconbar: {
    justifyContent: 'center',
    flex:0.4
  },
  iconDetail: {
    fontSize: 30,
    color: '#F99601',
  },
});
