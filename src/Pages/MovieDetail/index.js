import React from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/Ionicons';
import moment from 'moment';
import ListActors from '../../Component/ListActors';
import Genres from '../Genres';

class MovieDetail extends React.Component {
  constructor() {
    super();
    this.state = {
      detailMovie: {},
      // detailCast:{detailMovie:{credits:{cast:[]}}},
      detailCast:[],
      isLoading: false,
    };
  }

  async componentDidMount() {
    this.setState({isLoading: true});
    const detailServer = await Axios.get(
      `http://code.aldipee.com/api/v1/movies/${this.props.route.params.idFilm}`
    );
    this.setState({
      detailMovie: detailServer.data,
      detailCast: detailServer.data.credits.cast,
    });
    // this.setState({detailCast: detailServer.data});
    this.setState({isLoading: false});
  }


  render() {
    // console.log(this.state.detailCast,'detailcast')
    return (
      <ScrollView showsVerticalScrollIndicator={false} style={{flex: 1}}>
        
        <View style={styles.Top}>
          <ImageBackground
            source={{uri: `${this.state.detailMovie.backdrop_path}`}}
            style={styles.header}>
            <View style={styles.viewIcon}>
              <View style={styles.iconLeft}>
                <TouchableOpacity
                  onPress={() => {
                    this.props.navigation.navigate('Home', {});
                  }}>
                  <Icon name="arrow-back-circle" style={styles.icon} />
                </TouchableOpacity>
              </View>
              <View style={styles.iconRight}>
                <TouchableOpacity>
                <Icon name="heart-circle" style={styles.icon} />
                </TouchableOpacity>
                <TouchableOpacity>
                 
                <Icon name="share-social-sharp" style={styles.icon} />
                </TouchableOpacity>
                
              </View>
            </View>
            </ImageBackground>
            <View style={styles.headMovie}>
              <Image
                source={{uri: `${this.state.detailMovie.poster_path}`}}
                style={styles.poster}
              />
              <View style={styles.movieShort}>
                <Text style={styles.judul}>{this.state.detailMovie.title}</Text>
                <Text style={styles.tagline}>"{this.state.detailMovie.tagline}"</Text>
                <Text style={styles.status}>{this.state.detailMovie.status}</Text>
                <Text style={styles.runtime}>Runtime : {this.state.detailMovie.runtime} mnt</Text>
                <Text style={styles.release}>Release date : {moment(this.state.detailMovie.release_date).format('DD MMM YYYY')}</Text>
                <View style={{flexDirection:'row'}}>
                  <Icon name="star" style={styles.star}/>
                  <Text style={styles.rating}>{this.state.detailMovie.vote_average}</Text>
                  </View>
              </View>
            </View>  
        </View>
        <View style={styles.Middle}>
          <View style={styles.Genres}>
            <Text style={styles.textGenres}>Genre :</Text>
            <View style={styles.listGenres}>
              <Genres />
            </View>
            <View style={styles.synopsis}>
            <Text style={styles.textSynopsis}>Synopsis : </Text>
            <Text style={styles.isiSynopsis}>{this.state.detailMovie.overview}</Text>
            </View>
          </View>
        </View>
        <View style={styles.Bottom}>
          {/* <ListActors /> */}
          <Text style={styles.textlistArtis}>Actors/Artist</Text>
          <View style={styles.listArtis}>
            {this.state.detailCast.map(item =>(
              <TouchableOpacity style={{margin: 5}}>
              <Image
                style={styles.artis}   
                source={{uri: `${item.profile_path}`}}
              />
              <Text style={styles.nameArtis}>{item.name}</Text> 
            </TouchableOpacity>
            ))}
            
          </View> 
         {/* </View> */}
        </View>
      </ScrollView>
    );
  }
}

export default MovieDetail;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  Container: {
    flex: 1,
  },
  Top: {
    flex: 1,
    backgroundColor:'#749498'
  },
  viewIcon: {
    flexDirection: 'row',
  },
  iconLeft: {
    flex: 1,
    marginLeft:-10,
  },
  iconRight: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  icon: {
    fontSize: 30,
    color: '#5364f2',
    marginLeft: 5,
    marginRight: 5,
  },
  header: {
    width: windowWidth,
    height: windowHeight * 0.3,
    paddingHorizontal: 25,
    paddingTop: 10,
  },
  headMovie:{
    flexDirection:'row',
    flex:1,
    marginHorizontal: 10,
    borderRadius: 10,
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.19,
    shadowRadius: 4.65,
    elevation: 10,
    marginTop: -windowHeight*0.13,
    flexDirection:'row',
    backgroundColor: 'rgba(21,21,21,0.5)',
    overflow: 'hidden',
  },
  poster: {
    height: 169.5,
    width: 112.5,
    borderRadius: 15,
    borderColor: 'grey',
    overflow: 'hidden',
    resizeMode: 'contain',
  },
  movieShort:{
    flex:3,
    marginLeft:10,
    justifyContent:'space-evenly',
    padding:15,
  },
  judul:{
    fontFamily:'Open Sans',
    fontSize:21,
    fontWeight:'bold',
    color:'#FFFFFF',
  },
  tagline:{
    fontFamily:'Open Sans',
    fontSize:14,
    fontWeight:'bold',
    color:'#DEDDDF',
  },
  status:{
    fontFamily:'Open Sans',
    fontSize:14,
    fontWeight:'bold',
    color:'#DEDDDF',
  },
  runtime:{
    fontFamily:'Open Sans',
    fontSize:14,
    fontWeight:'bold',
    color:'#DEDDDF',
  },
  release:{
    fontFamily:'Open Sans',
    fontSize:14,
    fontWeight:'bold',
    color:'#DEDDDF',
  },
  star:{
    color:'#F99601',
    justifyContent:'space-around',
    marginRight:5
  },
  rating:{
    fontFamily:'Open Sans',
    fontSize:14,
    fontWeight:'bold',
    color:'#DEDDDF',
  },
  Middle: {
    backgroundColor: '#749498',
    flex: 1,
    padding: 10,
  },
  Genres:{
    flex:1,
    padding:15
  },
  textGenres:{
  },
  listGenres:{
    flexDirection:'row',
    marginLeft:15
  },
  synopsis:{
    flex:3,
    marginTop:15,
    marginBottom:15
  },
  textSynopsis:{
    fontFamily:'Open Sans',
    fontSize:18,
    fontWeight:'900',
    color:'#0F031C'
  },
  isiSynopsis:{
    fontFamily:'Open Sans',
    fontSize:14,
    fontWeight:'900',
    color:'#0F031C',
    marginTop:5
  },

  Bottom: {
    backgroundColor: '#749498',
    flex: 1,
  },
  textlistArtis: {
    marginTop: 10,
    marginLeft: 10,
    fontSize: 18,
    fontWeight: 'bold',
  },
  listArtis: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
    margin: 10,
  },
  artis: {
    height: 150,
    width: 100,
    borderRadius: 10,
  },
  nameArtis: {
    fontSize: 13,
  },
});
