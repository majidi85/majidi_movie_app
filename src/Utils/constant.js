export const WARNA_1 = '#dfe5fc';
export const WARNA_2 = '#afb6f4';
export const WARNA_3 = '#5ca6fb';
export const WARNA_4 = '#165bfe';
export const WARNA_5 = '#af5fce';
export const WARNA_6 = '#4f3639';
export const WARNA_7 = '#5364f2';