this.state = {
    isLoading: false,
    title: '',
    tagline: '',
    status: '',
    releaseDate: '',
    overview: '',
    poster: null,
    backdrop: null,
    voteAverage: 0,
    runtime: 0,
    genres: [],
    synopsis: '',
    cast: [],
  };


  //ambil data
  getMovieDetail = async () => {
    this.setState({isLoading: true});
    const movie = await axios.get(
      'http://code.aldipee.com/api/v1/movies/' + this.props.route.params,
    );
    this.setState({
      title: movie.data.original_title,
      tagline: movie.data.tagline,
      status: movie.data.status,
      cast: movie.data.credits.cast,
      genres: movie.data.genres,
      runtime: movie.data.runtime,
      overview: movie.data.overview,
      voteAverage: movie.data.vote_average,
      releaseDate: movie.data.release_date,
      poster: movie.data.poster_path,
      backdrop: movie.data.backdrop_path,
    });

    this.setState({isLoading: false});
  };



  //nampilinnya
  {this.state.cast
    .map(item => (
      <TouchableOpacity
        key={item.id}
        style={{
          height: 170,
        }}>
        <Image
          source={{uri: item.profile_path}}
          style={Styles.castImg}
        />
        <Text style={Styles.castName}>{item.name}</Text>
      </TouchableOpacity>
    ))
   }