import React from 'react';
import {Button, StyleSheet, Text, View} from 'react-native';

class ListMovie extends React.Component {
  render() {
    return (
      <View>
        <Image source={PosterMovie} style={styles.PosterMovie}/>
        <View style={styles.text}>
            <Text style={styles.JudulMovie}>{JudulMovie}</Text>
            <Text style={styles.ReleaseDate}>{ReleaseDate}</Text>
            <Text style={styles.Rating}>{Rating}</Text>
            <Text style={styles.Genre}>{Genre}</Text>
            <Button title="show more.."style={styles.showMore} />
        </View>
      </View>
    );
  }
}

export default ListMovie;

const styles = StyleSheet.create({
    PosterMovie:{
        maxHeight:180,
        maxWidth:180,
        borderRadius:15,
        borderWidth:0.2,
        borderColor:'grey',
        overflow:"hidden"
    },
    text:{
        marginLeft:10,
        flex:1,
        alignItems:'flex-start'
    },
    JudulMovie:{
        fontSize:20,
        fontWeight:'bold',
        padding:3
    },
    ReleaseDate:{},
    Rating:{
        fontSize:14,
    },
    Genre:{
        fontSize:14,
    },
    showMore:{
        fontSize:18,
        fontWeight:'900',
    },
});
