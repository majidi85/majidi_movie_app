import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

class ListActors extends React.Component {
  constructor() {
    super();
    this.state = {
        isLoading: false,
        cast: [],
    };
  }

  getActors = async () => {
    this.setState({isLoading: true});
    const actors = await axios.get(
      'http://code.aldipee.com/api/v1/movies/' + this.props.route.params,
    );
    this.setState({
      cast: actors.data.credits.cast,
    });
    this.setState({isLoading: false});
  };

  render() {
    return (
    //   <View style={styles.Bottom}>
    <View>
        <Text style={styles.textlistArtis}>Actors/Artist</Text>
        <View style={styles.listArtis}>
          {this.state.cast.map(item => (
            <TouchableOpacity
              key={item.id}
              >
              <Image style={styles.artis} source={{uri: item.profile_path}}/>
              <Text style={styles.nameArtis}>{item.name}</Text>
            </TouchableOpacity>
          ))}
        </View>
      </View>
    );
  }
}

export default ListActors;

const styles = StyleSheet.create({
    Bottom: {
        backgroundColor: '#749498',
        flex: 4,
      },
      textlistArtis: {
        marginTop: 10,
        marginLeft: 10,
        fontSize: 18,
        fontWeight: 'bold',
      },
      listArtis: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        margin: 10,
      },
      artis: {
        height: 80,
        width: 80,
        borderRadius: 10,
      },
      nameArtis: {
        fontSize: 13,
      },
});
