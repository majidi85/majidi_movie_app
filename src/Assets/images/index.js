import Logo from './logo.png';
import SplashBackground from './background.png';
import A1 from './a1.jpeg';
import Shadow from './Shadow.png';
import Majidi from './Majidi.png';
import NetroCinema from './NetroCinema.png';
import NetroCinema2 from './NetroCinema2.png'

export {Logo,A1, Shadow,SplashBackground,Majidi,NetroCinema,NetroCinema2};