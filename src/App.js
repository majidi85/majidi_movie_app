import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import Router from './Router'
import { MovieDetail } from './Pages';
import Genres from './Pages/Genres';
//import { MovieDetail } from './Pages';


class App extends React.Component {
  constructor() {
    super();
  }

  render() {
    return (
      <NavigationContainer>
        <Router />
        {/* <MovieDetail /> */}
        {/* <Genres /> */}
      </NavigationContainer>
    )
  }
}

export default App

